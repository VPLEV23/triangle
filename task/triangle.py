def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    #Check if triangle inequality is True and all sides are positive
    return all([a + b > c, a + c > b, b + c > a]) and all ([a > 0, b > 0, c > 0])
